class UsersController < ApplicationController
  
  # :editと:updateだけに適応　only
  before_action :signed_in_user, only: [:index, :edit, :update, :destroy, :message, :image]
  before_action :correct_user,   only: [:edit, :update, :message, :image]
  before_action :admin_user,     only: :destroy

  def message
    @user = User.find(params[:id])
    @messages = @user.messages.paginate(page: params[:page])
  end

  def image
    @image = Image.new
    @user = User.find(params[:id])
    @images = @user.images.paginate(page: params[:page])

  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
    @message = current_user.messages.build
  end

  def new
  	@user = User.new
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User destroyed."
    redirect_to users_url
  end

  def edit
    @user = User.find(params[:id])
  end


  def update
    @user = User.find(params[:id])
    #render 'sessions/auth'
    if @user.update_attributes(user_update_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  def create
  @user = User.new(user_params)
  
     # KMD が含まれているメールアドレス以外拒否
     if @user.email.include?("@kmd.keio.ac.jp")
       
          # KMDが含まれていたらデータベースに登録
          if @user.save
            sign_in @user
            flash[:success] = "Welcome, You can change (Account -> Setting) the profile!!!"
            redirect_to @user
          else
            render 'new'
          end

     else

        #含まれていなかったらエラー
         flash[:error] = "Email unauthorized."
         render 'new'
     
     end

  end

  def destroy
    sign_out
    redirect_to root_url
  end
  
  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  private

    def user_params
      params.require(:user).permit(:name, :email, :student_id, :password,
                                   :password_confirmation)
    end

    def user_update_params
      params.require(:user).permit(:name, :description, :password,
                                   :password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end
   
    def admin_user
      redirect_to(root_path) unless current_user.admin?
    end
end