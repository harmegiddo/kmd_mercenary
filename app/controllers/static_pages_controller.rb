class StaticPagesController < ApplicationController
  def home
    if signed_in?
      # session helper, ようは自分, model/userから貰ってくる
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      else
      @user = User.new
    end



  end

  def about
  end
  
  def help
  end

  def contact
  end

end
