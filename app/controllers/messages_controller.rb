class MessagesController < ApplicationController
  before_action :signed_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @message = current_user.messages.build(message_params)
    message = params[:message]
    @temp = message['originator_id']
    @message.originator_id = @message.user_id
    @message.user_id = @temp
    
    if @message.save
      flash[:success] = "message sended!"
      redirect_to root_url
    else
      #@feed_items = []
      #render message_user_path(message.originator_id)
      redirect_to root_url
      flash[:error] = "message not sended!"
    end
  end

  def destroy
    @message.destroy
    redirect_to root_url
  end

  private

    def message_params
      params.require(:message).permit(:content)
    end

    def correct_user
      @message = current_user.messages.find_by(id: params[:id])
      redirect_to root_url if @message.nil?
    end
end