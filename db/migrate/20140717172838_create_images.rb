class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer :user_id
      t.string :filename
      t.binary :file

      t.timestamps
    end
      add_index :images, [:user_id, :created_at]
  end
end
